import {
	Command,
	CommandDecorators,
	Logger,
	logger,
	Message,
	Middleware
} from '@yamdbf/core';

import { ShardClient } from '../master';

const { resolve, expect } = Middleware;
const { using } = CommandDecorators;

export default class extends Command<ShardClient> {
	@logger('Command')
	private readonly _logger: Logger;

	public constructor() {
		super({
			name: 'custom',
			aliases: ['c'],
			desc: 'Manage custom bot instances',
			usage: '<prefix>custom action memberId discordToken',
			ownerOnly: true,
			guildOnly: false
		});
	}

	@using(resolve('action: string, mId: string, sId: string, dToken: string'))
	@using(expect('action: string, mId: string, sId: string, dToken: string'))
	public async action(
		message: Message,
		[action, memberId, serverId, token]: [string, string, string, string]
	): Promise<any> {
		if (Object.values(this.client.processMap).indexOf(serverId) === -1) {
			return message.channel.send('Invalid server id');
		}

		const user = await this.client.users.fetch(memberId, false);
		if (!user) {
			return message.channel.send('Invalid user id');
		}

		if (action === 'create') {
			this.client.sendToServer(
				serverId,
				{ id: message.id, cmd: 'custom', args: [action, memberId, token] },
				content => {
					if (content.error) {
						message.channel.send(content.data);
					} else {
						message.channel.send(
							`Successfully started custom bot for ${memberId} on ${serverId}`
						);
					}
				}
			);
		} else if (action === 'delete') {
			return message.channel.send('Not implemented');
		}
	}
}
