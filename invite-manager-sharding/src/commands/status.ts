import {
	Command,
	CommandDecorators,
	Logger,
	logger,
	Message,
	Middleware
} from '@yamdbf/core';
import moment from 'moment';
import { ProcessDescription } from 'pm2';

import { ShardClient } from '../master';

const { resolve } = Middleware;
const { using } = CommandDecorators;

export default class extends Command<ShardClient> {
	@logger('Command')
	private readonly _logger: Logger;

	private timer: NodeJS.Timer;

	public constructor() {
		super({
			name: 'status',
			aliases: ['s'],
			desc: 'Get the status of pm2 processes',
			usage: '<prefix>status (action)',
			callerPermissions: ['BAN_MEMBERS'],
			guildOnly: false
		});
	}

	@using(resolve('action: string'))
	public async action(message: Message, [action]: [string]): Promise<any> {
		if (action === 'stop') {
			clearInterval(this.timer);
			message.channel.send('Stopping live status updates');
			return;
		}

		const msg = (await message.channel.send(
			'Collecting shard status...'
		)) as Message;

		const texts: { [x: string]: string } = {};
		const func = () =>
			this.client.sendToAllServers(
				{ id: message.id, cmd: 'processes' },
				content => {
					let text = 'Last update: ' + moment().calendar() + '\n\n';

					text += `**${content.server}**\n`;
					text += '```\n';
					let used = 0;
					const cols: string[][] = [];
					const widths = [0, 0, 0, 0];
					content.processes.forEach((proc: ProcessDescription) => {
						used += proc.monit.memory;
						const newCol = [
							proc.pm2_env.status === 'online' ? '✓' : '❌',
							proc.name,
							proc.monit.cpu.toFixed(0) + '%',
							(proc.monit.memory / 1000000).toFixed(0) + ' MB',
							moment(proc.pm2_env.pm_uptime).fromNow()
						];
						cols.push(newCol);
						for (let i = 0; i < newCol.length; i++) {
							if (newCol[i].length > widths[i]) {
								widths[i] = newCol[i].length;
							}
						}
					});
					cols.forEach(col => {
						for (let i = 0; i < col.length; i++) {
							text += col[i] + ' '.repeat(widths[i] - col[i].length);
							if (i < col.length - 1) {
								text += ' | ';
							}
						}
						text += '\n';
					});
					const ratio = used / content.memory;
					text +=
						'\n\nMemory [' +
						'='.repeat(ratio * 20) +
						' '.repeat(20 - ratio * 20) +
						'] ' +
						(ratio * 100).toFixed(0) +
						'% ' +
						(used / 1000000000).toFixed(2) +
						' GB / ' +
						(content.memory / 1000000000).toFixed(2) +
						' GB\n';
					text += '```\n';

					texts[content.server] = text;

					let newText = '';
					Object.keys(texts)
						.sort()
						.forEach(k => (newText += texts[k]));
					msg.edit(newText);
				}
			);

		func();

		if (action === 'start') {
			this.timer = setInterval(func, 5000);
		}
	}
}
