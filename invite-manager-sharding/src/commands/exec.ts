import {
	Command,
	CommandDecorators,
	Logger,
	logger,
	Message,
	Middleware
} from '@yamdbf/core';

import { ShardClient } from '../master';

const { resolve, expect } = Middleware;
const { using } = CommandDecorators;

export default class extends Command<ShardClient> {
	@logger('Command')
	private readonly _logger: Logger;

	public constructor() {
		super({
			name: 'exec',
			aliases: ['e'],
			desc: 'Execute command',
			usage: '<prefix>exec cmd',
			ownerOnly: true,
			guildOnly: false
		});
	}

	@using(resolve('cmd: string'))
	@using(expect('cmd: string'))
	public async action(message: Message, [cmd]: [string]): Promise<any> {
		message.channel.send('Not implemented');
	}
}
