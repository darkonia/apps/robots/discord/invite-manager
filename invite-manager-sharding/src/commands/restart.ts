import {
	Command,
	CommandDecorators,
	Logger,
	logger,
	Message,
	Middleware
} from '@yamdbf/core';

import { ShardClient } from '../master';

const { resolve, expect } = Middleware;
const { using } = CommandDecorators;

export default class extends Command<ShardClient> {
	@logger('Command') private readonly _logger: Logger;

	public constructor() {
		super({
			name: 'restart',
			aliases: ['r'],
			desc: 'Restart a process',
			usage: '<prefix>restart name',
			ownerOnly: true,
			guildOnly: false
		});
	}

	@using(resolve('name: string'))
	@using(expect('name: string'))
	public async action(message: Message, [name]: [string]): Promise<any> {
		const serverId = this.client.processMap[name];
		if (!serverId) {
			message.channel.send(
				'I do not know which server that process is running on'
			);
			return;
		}

		const msg = (await message.channel.send(
			`Restarting ${name} on ${serverId} ...`
		)) as Message;

		this.client.sendToServer(
			serverId,
			{ id: message.id, cmd: 'restart', args: [name] },
			content => {
				let text = '';
				if (content.error) {
					text = `Could not restart process: ${content.error}`;
				} else {
					text = `Restarted ${name} on ${serverId}`;
				}
				msg.edit(text);
			}
		);
	}
}
