import {
	Command,
	CommandDecorators,
	Logger,
	logger,
	Message,
	Middleware
} from '@yamdbf/core';

import { ShardClient } from '../master';

const { resolve, expect } = Middleware;
const { using } = CommandDecorators;

export default class extends Command<ShardClient> {
	@logger('Command')
	private readonly _logger: Logger;

	public constructor() {
		super({
			name: 'logs',
			aliases: ['l'],
			desc: 'Stream logs from processes',
			usage: '<prefix>logs action (level) (process)',
			callerPermissions: ['BAN_MEMBERS'],
			guildOnly: false
		});
	}

	@using(resolve('action: string, level: string, proc: string'))
	@using(expect('action: string'))
	public async action(
		message: Message,
		[action, level, proc]: [string, string, string]
	): Promise<any> {
		let serverId = null;
		if (proc) {
			serverId = this.client.processMap[proc];
			if (!serverId) {
				message.channel.send(
					'I do not know which server that process is running on'
				);
				return;
			}
		}

		if (level && level !== 'out' && level !== 'error') {
			message.channel.send('Invalid log level. Use one of: `out`, `error`');
			return;
		}

		if (action === 'start') {
			if (serverId) {
				await message.channel.send(
					`Streaming logs from ${proc} on ${serverId}...`
				);
				this.client.sendToServer(
					serverId,
					{ id: message.id, cmd: 'logs', args: [action, level, proc] },
					content => message.channel.send('```' + content.data + '```')
				);
			} else {
				await message.channel.send(`Streaming logs from all processes...`);
				this.client.sendToAllServers(
					{ id: message.id, cmd: 'logs', args: [action, level] },
					content => message.channel.send('```' + content.data + '```')
				);
			}
		} else if (action === 'stop') {
			if (serverId) {
				message.channel.send(`Stopped logs stream from ${proc} on ${serverId}`);

				this.client.sendToServer(serverId, {
					id: message.id,
					cmd: 'logs',
					args: [action, '', proc]
				});
			} else {
				message.channel.send(`Stopped all log streams`);

				this.client.sendToAllServers({
					id: message.id,
					cmd: 'logs',
					args: [action]
				});
			}
		} else {
			message.channel.send('Unknown action. Use `start` or `stop`.');
		}
	}
}
