import amqplib from 'amqplib';
import child_process from 'child_process';
import fs from 'fs';
import os from 'os';
import pm2 from 'pm2';

const config = require('../config.json');

amqplib.connect(config.rabbitmq).then(async conn => {
	const serverId = config.serverId;
	const qName = 'server-' + serverId;

	console.log('Connected to RabbitMQ!');

	pm2.connect(e => {
		if (e) {
			console.log(e);
			process.exit(2);
		}

		console.log('Connected to PM2!');

		let errProc: string = null;
		let errFunc: (data: any) => void = null;
		let outProc: string = null;
		let outFunc: (data: any) => void = null;
		pm2.launchBus((err, bus) => {
			if (err) {
				console.log(err);
				process.exit(2);
			}

			console.log('Launched PM2 bus!');

			bus.on('log:out', (data: any) => {
				if (outFunc) {
					if (!outProc || outProc === data.process.name) {
						outFunc(data.data);
					}
				}
			});

			bus.on('log:err', (data: any) => {
				if (errFunc) {
					if (!errProc || errProc === data.process.name) {
						errFunc(data.data);
					}
				}
			});
		});

		conn.createChannel().then(async channel => {
			await channel.assertQueue(qName, {
				autoDelete: true,
				durable: false
			});

			await channel.assertQueue('shards-master', {
				durable: true
			});

			await channel.assertExchange('shards', 'fanout', {
				durable: true
			});

			await channel.bindQueue(qName, 'shards', '');

			console.log('RabbitMQ channel ready!');

			await channel.prefetch(5);
			channel.consume(
				qName,
				msg => {
					const content = JSON.parse(msg.content.toString());
					const id: string = content.id;
					const cmd: string = content.cmd.toLowerCase();
					const args: string[] = content.args;

					console.log('Received: ' + JSON.stringify(content));

					if (cmd === 'processes') {
						pm2.list((err, descriptions) => {
							if (err) {
								console.log(err);
								return;
							}

							channel.sendToQueue(
								'shards-master',
								Buffer.from(
									JSON.stringify({
										id,
										server: serverId,
										memory: os.totalmem(),
										processes: descriptions
									})
								)
							);
						});
					} else if (cmd === 'restart') {
						const procName = args[0];
						pm2.restart(procName, (err, proc) => {
							if (err) {
								channel.sendToQueue(
									'shards-master',
									Buffer.from(
										JSON.stringify({
											id,
											server: serverId,
											error: err
										})
									)
								);
								return;
							}

							channel.sendToQueue(
								'shards-master',
								Buffer.from(
									JSON.stringify({
										id,
										server: serverId,
										process: proc
									})
								)
							);
						});
					} else if (cmd === 'logs') {
						const action = args[0];
						const level = args[1];
						const proc = args[2];

						if (action === 'start') {
							const newProc = proc ? proc : null;
							const newFunc = (data: any) => {
								if (!data) {
									return;
								}

								channel.sendToQueue(
									'shards-master',
									Buffer.from(
										JSON.stringify({
											id,
											server: serverId,
											data
										})
									)
								);
							};

							if (level === 'out') {
								outProc = newProc;
								outFunc = newFunc;
							} else if (level === 'error') {
								errProc = newProc;
								errFunc = newFunc;
							}
						} else if (action === 'stop') {
							errFunc = null;
							errProc = null;
							outFunc = null;
							outProc = null;
						}
					} else if (cmd === 'exec') {
						const c = args[0];

						child_process.exec(c, (err, out) => {
							channel.sendToQueue(
								'shards-master',
								Buffer.from(
									JSON.stringify({
										id,
										server: serverId,
										err,
										out
									})
								)
							);
						});
					} else if (cmd === 'custom') {
						const action = args[0];
						const memberId = args[1];
						const token = args[2];

						// Update bot pm2 json
						const pm2Bot = JSON.parse(
							fs.readFileSync(config.customBotDir + 'pm2.json', {
								encoding: 'utf-8'
							})
						);
						pm2Bot.apps.push({
							name: `bot-${memberId}`,
							script: 'bin/bot.js',
							args: `${token} 1 1`,
							log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS'
						});
						fs.writeFileSync(
							config.customBotDir + 'pm2.json',
							JSON.stringify(pm2Bot, null, 2)
						);

						// Start bot process
						pm2.start(
							{
								name: `bot-${memberId}`,
								script: 'bin/bot.js',
								args: `${token} 1 1`,
								log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS',
								cwd: config.customBotDir
							},
							(err, proc) => {
								if (err) {
									channel.sendToQueue(
										'shards-master',
										Buffer.from(
											JSON.stringify({
												id,
												server: serverId,
												error: err
											})
										)
									);
									return;
								}

								channel.sendToQueue(
									'shards-master',
									Buffer.from(
										JSON.stringify({
											id,
											server: serverId,
											process: proc
										})
									)
								);
							}
						);

						// Update tracker pm2 json
						const pm2Tracker = JSON.parse(
							fs.readFileSync(config.customTrackerDir + 'pm2.json', {
								encoding: 'utf-8'
							})
						);
						pm2Tracker.apps.push({
							name: `tracker-${memberId}`,
							script: 'build/inviter-tracker.js',
							args: `${token} 1 1`,
							log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS'
						});
						fs.writeFileSync(
							config.customTrackerDir + 'pm2.json',
							JSON.stringify(pm2Tracker, null, 2)
						);

						// Start tracker process
						pm2.start(
							{
								name: `tracker-${memberId}`,
								script: 'build/invite-tracker.js',
								args: `${token} 1 1`,
								log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS',
								cwd: config.customTrackerDir
							},
							(err, proc) => {
								if (err) {
									channel.sendToQueue(
										'shards-master',
										Buffer.from(
											JSON.stringify({
												id,
												server: serverId,
												error: err
											})
										)
									);
									return;
								}

								channel.sendToQueue(
									'shards-master',
									Buffer.from(
										JSON.stringify({
											id,
											server: serverId,
											process: proc
										})
									)
								);
							}
						);
					}

					channel.ack(msg, false);
				},
				{
					noAck: false
				}
			);
		});
	});
});
