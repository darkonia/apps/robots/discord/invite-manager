import { Client } from '@yamdbf/core';
import amqplib from 'amqplib';
import path from 'path';
import { ProcessDescription } from 'pm2';

const config = require('../config.json');

export interface RabbitMqData {
	id: string;
	cmd: string;
	server: string;

	[x: string]: any;
}

export class ShardClient extends Client {
	public conn: amqplib.Connection;
	public channel: amqplib.Channel;

	public serverIds: string[];
	public processMap: { [x: string]: string };

	public responses: {
		[id: string]: {
			listener: (
				newData: RabbitMqData,
				data: RabbitMqData[],
				pending: string[]
			) => void;
			servers: string[];
			data: RabbitMqData[];
		};
	};

	public constructor(conn: amqplib.Connection) {
		super(
			{
				commandsDir: path.join(__dirname, 'commands'),
				token: config.discordToken,
				owner: config.owners,
				ratelimit: '2/5s',
				disableBase: ['setlang', 'eval', 'eval:ts'],
				unknownCommandError: false
			},
			{
				apiRequestMethod: 'burst',
				disabledEvents: ['TYPING_START', 'USER_UPDATE', 'PRESENCE_UPDATE'],
				messageCacheMaxSize: 2,
				messageCacheLifetime: 10,
				messageSweepInterval: 30,
				restWsBridgeTimeout: 20000,
				ws: {
					compress: true
				}
			}
		);

		this.serverIds = [];
		this.processMap = {};
		this.responses = {};

		this.conn = conn;
		this.setDefaultSetting('prefix', '+');

		// Setup RabbitMQ channels
		conn.createChannel().then(async channel => {
			this.channel = channel;

			await channel.assertQueue('shards-master', {
				durable: true
			});

			await channel.assertExchange('shards', 'fanout', {
				durable: true
			});

			await channel.prefetch(5);
			channel.consume(
				'shards-master',
				msg => {
					const content = JSON.parse(msg.content.toString()) as RabbitMqData;
					channel.ack(msg, false);

					console.log(`Received: ${content.id} | ${content.server}`);

					if (content.id === 'initial') {
						this.serverIds.push(content.server);
						content.processes.forEach((proc: ProcessDescription) => {
							this.processMap[proc.name] = content.server;
						});
						return;
					}

					const resp = this.responses[content.id];
					if (resp) {
						resp.data.push(content);
						resp.servers = resp.servers.filter(s => s !== content.server);
						resp.listener(content, resp.data, resp.servers);
					} else {
						console.error('RECEIVED INVALID RESPONSE ID ' + content.id);
					}
				},
				{
					noAck: false
				}
			);

			this.sendToAllServers({ id: 'initial', cmd: 'processes' });
		});
	}

	public sendToAllServers(
		msg: { cmd: string; id: string; args?: string[] },
		listener?: (
			newData: RabbitMqData,
			data: RabbitMqData[],
			pending: string[]
		) => void
	) {
		console.log(`Sending to all: ${JSON.stringify(msg)}`);

		if (listener) {
			this.responses[msg.id] = {
				listener,
				servers: [...this.serverIds],
				data: []
			};
		}
		this.channel.publish('shards', '', Buffer.from(JSON.stringify(msg)));
	}

	public sendToServer(
		server: string,
		msg: { cmd: string; id: string; args?: string[] },
		listener?: (
			newData: RabbitMqData,
			data: RabbitMqData[],
			pending: string[]
		) => void
	) {
		console.log(`Sending to ${server}: ${JSON.stringify(msg)}`);

		if (listener) {
			this.responses[msg.id] = {
				listener,
				servers: [server],
				data: []
			};
		}
		this.channel.sendToQueue(
			'server-' + server,
			Buffer.from(JSON.stringify(msg))
		);
	}
}

amqplib.connect(config.rabbitmq).then(async conn => {
	const client = new ShardClient(conn);
	client.start();
});
